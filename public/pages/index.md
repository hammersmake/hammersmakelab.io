<!--
.. title: index
.. slug: index
.. date: 2020-05-31 23:23:14 UTC+10:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

﻿<h2>William Mckee</h2>

![me](galleries/me.svg)

<h2>IT Consulting<br>
and Services</h2>

I bring a range of skills and guarantee a quantifiable, durable improvement to your process. Receive a free first consultation.

![pi](galleries/pi.jpeg)

<iframe class="mj-w-res-iframe" frameborder="0" scrolling="yes" marginheight="0" marginwidth="0" src="https://app.mailjet.com/widget/iframe/4AoC/jFC" width="100%"></iframe>

<script type="text/javascript" src="https://app.mailjet.com/statics/js/iframeResizer.min.js"></script>

<h3>Skills</h3>

* Programmer 

A focus on backend programming - Python, API and database development. Over the years attending hackathons, working on open source and commercial software projects. Code can be found on [GitLab](https://gitlab.com/hammersmake) 

![world](/galleries/world.gif)

SpaceGIF: A Python library/API for creating GIFs from text user input and NASA space photography. 

* Digital Drawing

10+ years of creation and publish of art - [artctrl.me](https://artctrl.me)

![hatgirl](galleries/hat-girl-bw.png)

Job roles have varied over the years - from a screenprinter for shirt.co.nz, to support worker for Ministry of Education, and API Developer for InfoTrack and Fusion Professionals, and as a SEO Developer at Fusion Professionals.  

* Speaker

As a firm believer in passing my knowledge, services are available as a speaker to both technical and leadership events. Together, we will tailor a program perfectly suited to the target audience and provide insight that will drive everyone to reach their personal best.

<iframe width="560" height="315" src="https://www.youtube.com/embed/RzG_tageR64" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
