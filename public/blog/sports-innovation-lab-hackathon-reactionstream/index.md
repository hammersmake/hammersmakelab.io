﻿<!--
.. title: sports innovation lab hackathon reactionstream
.. slug: sports-innovation-lab-hackathon-reactionstream
.. date: 2020-06-01 00:49:32 UTC+10:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

Since I had been working a full time job as a SEO Developer on closed source code - i have not really been getting along to hackathons. I guess I did a big burst back in Aug/Sept/Oct 2019. 

There have been more online virtual hackathons now that covid-19 is a thing. Normally these hackathons would run locally in a city, maybe nationwide - maybe several countries. With Covid-19 stay at home restrictions a number of these hackathons make it more friendly for those not in the countries where the events are hosted can join. I hope that the virtual hackathons remain a thing. 

I've gotten along to a few sports hackathons in the past. I have even gotten on a plane and travelled to Melbourne for a hackathon. 

I attended [Sports Innovation Lab Hackathon](https://www.sportsilab.com/hackathon) on the 21-22 May, from home. I had quite a large team, and mostly worked on the code for it. My favourite aspect about the hackathon was the zoom calls from organisers and mentors, meetings with team, and interviews with mentors on the teams ideas/product and of course - writing the code. 

[reactionstream project on gitlab](https://gitlab.com/hammersmake/reactionstream)

It is called Reaction Stream because it is a way for users to react to a video clip that they have seen. Their audio and video reaction to the video is overlay. They can also overlay a third post event reaction. 

I used boto3 to upload merged images to a s3 bucket.

PIL is another library using - to open up two images and perform alpha_composite on them - merging them together. This is used to feed a video overlay a image, or vise versa. PIL is also used to resize a image based on the size of another image. ImageIO is used for generating a gif. 
