<!--
.. title: freelancer hackathon 2019
.. slug: freelancer-hackathon-2019
.. date: 2019-12-01 23:16:18 UTC+10:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text
-->

This is the first post that I am going to do to talk about hackathon events that I attend. I've attended many over the years but never really reflected on them.

This was my 2nd time attending StartCon in Sydney at the Randwick Race Course. Last time was in 2018. The hackathon was in the same location and it was structured similar (though it felt like less people this year which was a shame). 

In 2018 I had a very large team that included Minji an excellent UX designer who I have worked with at several hackathons. In 2019 I wanted a smaller team but I didn't really want to work by myself (it's been over a year since I have done a solo hackathon). I had someone join my team on the first day of this hackathon. Sadly they  were unable to return for the 2nd day. I really must check with people to make sure they are going to be attending the whole hackathon before I join their team. Still it was more pleasant than having a large team and I didn't have to relie on others so much. 

My idea for the Freelancer Hackathon 2019 was to rebuilt their API using FastAPI. I wasn't really interested in adding new features, I just wanted to improve upon the developer experience. FastAPI offers OpenAPI generation auto. You just have the run the API and go to the /doc endpoint and it's all there for you - interactive OpenAPI docs. The current Freelancer docs just have curl commands which need to be copy and pasted into a terminal window to use (along with adding API key). I was pleased with what I built and was able to show Freelancer what their API could look like interactive. I did not place (I didn't expect to being a team of 1), but at least I built code and had a working demo. Most teams didn't have code and just had an idea with mockups. This is okay but I really like to build something to demo. 

I emailed Freelancer afterwards with the pitch to hire me on a contract bases in order to implement better developer docs. Sadly my contact there never emailed me back. I hope they consider building interactive openapi docs, even if I don't do it. 

Overall it was a great hackathon and a fun experience. I got to see alot of booths in the hallway and attended no talks. 

I hope to attend Freelancer API Hackathon next year - hopefully with a slightly larger team or a team that will stick around and do more with Freelancer API and FastAPI.

The slides for the talk [freelancer-fastapi](https://wmckee.com.au/freelancerfastapi.svg) 

![startcon19](/galleries/startcon19.jpg)

Group photo of all the teams.


